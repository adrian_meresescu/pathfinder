<?php

/**
 * FeedingGrounds
 *
 *
 * @package    FeedingGrounds
 * @subpackage Library
 * @author     Adrian Meresescu <adrian.meresescu@gmail.com>
 */
class FeedingGrounds
{
    /** @var int $rowCount - stores the number of rows */
    public $rowCount;

    /** @var int $colCount - stores the number of columns */
    public $colCount;

    /** @var  array $grid - stores the input map */
    public $grid;

    /** @var int $bestScore - largest score possible */
    private $bestScore = 0;

    /** @var  string $bestPath - the paths the hunter passes */
    private $bestPath;

    /**
     * FeedingGrounds constructor.
     * Sets the grids and calculates the rows and columns count
     *
     * @param array $grid
     *
     * @todo would be nice to validate the array (eg. 2D matrix, non-numeric values) but for the purpose of this demo we shall skip
     */
    public function __construct($grid)
    {
        $this->grid     = $grid;
        $this->rowCount = count($grid);
        $this->colCount = count(current($grid));

    }

    /**
     * Forage
     *
     * Using recursion, generate all possible paths
     *
     * @param int    $currentRow
     * @param int    $currentColumn
     * @param string $path
     * @param int    $score
     *
     * @todo include the coordinates in the footer (x,y) to have possibility to draw the map
     */
    public function forage($currentRow, $currentColumn, $path = '', $score = 0)
    {

        // travel to the right
        if ($currentRow == ($this->rowCount - 1)) {
            for ($i = $currentColumn; $i < $this->colCount; $i++) {
                $path .= '-' . $this->grid[$currentRow][$i];
                $score += $this->grid[$currentRow][$i];
            }

            // checkpoint reached, try to update the path and score
            if ($score > $this->bestScore) {
                $this->bestScore = $score;
                $this->bestPath  = $path;
            }

            return;
        }

        // travel down
        if ($currentColumn == ($this->colCount - 1)) {
            for ($i = $currentRow; $i <= $this->rowCount - 1; $i++) {
                $path .= '-' . $this->grid[$i][$currentColumn];
                $score += $this->grid[$i][$currentColumn];
            }

            // checkpoint reached, try to update the path and score
            if ($score > $this->bestScore) {
                $this->bestScore = $score;
                $this->bestPath  = $path;
            }

            return;
        }

        // add current position to path
        $path = $path . '-' . $this->grid[$currentRow][$currentColumn];

        // add current points
        $score += $this->grid[$currentRow][$currentColumn];

        // go one block down
        $this->forage($currentRow + 1, $currentColumn, $path, $score);

        // go one block right
        $this->forage($currentRow, $currentColumn + 1, $path, $score);
    }

    /**
     * Returns the best score
     *
     * @return int
     */
    public function getBestScore()
    {
        return $this->bestScore;
    }

    /**
     * Returns the best Path
     *
     * @return string
     */
    public function getBestPath()
    {
        return $this->bestPath;
    }

}