<?php

require_once(__DIR__ . '/FeedingGrounds.php');

$grid = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
];

$feedingTime = new FeedingGrounds($grid);
$feedingTime->forage(0, 0);

echo 'Best score = ' . $feedingTime->getBestScore() . '<br>';
echo 'Best Path = ' . $feedingTime->getBestPath();