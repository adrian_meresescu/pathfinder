# Pathfinder

## Synopsis

Given a 2 dimensional array a hungry forager needs to find the path with the maximum score. Each cell contains a number greater than 0 and the forager can move down or sideways.
Generate an script that identifies the path with the maximum score.

## Code Example

This is how the input array should look like

[1,2,3]
[4,5,6]
[2,8,9]

The Best path is 1 -> 4 -> 7 -> 8 -> 9 summing up to 29 points.

## Installation

Clone the repo and run example.php.

## Testing


```php
$pathFinder  = new FeedingGrounds($grid)
$feedingTime->forage(0, 0);


echo 'Best score = ' . $feedingTime->getBestScore();
echo 'Best Path  = ' . $feedingTime->getBestPath();
```

Or, you can just take a peek at example.php.
